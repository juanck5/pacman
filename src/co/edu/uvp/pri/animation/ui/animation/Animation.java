package co.edu.uvp.pri.animation.ui.animation;


import co.edu.uvp.pri.animation.Pacman;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.Timer;


public class Animation extends JPanel {

    private Timer timer = null;
    private short pacman = 300;
    private short angulointerno = 30;
    private short pacmanTamaño = 20;
    private int bocaAbierta = 1;
    private int pacmanx = 100;
    private int pacmany = 100;
    private int pacmanp = 0;
    private BufferedImage [] image;
    private Pacman logica = new Pacman();
    

    
    

    
    
    public Animation() {
        super();
        super.setFocusable(true);
        super.setRequestFocusEnabled(true);
        super.requestFocus();
        super.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                
            }

            @Override
            public void keyPressed(KeyEvent e) {
                //if()
                
                
                
                if(e.getKeyCode() == KeyEvent.VK_RIGHT){
                    logica.mover(Pacman.Direccion.DERECHA);
                    pacmanp = 0;
                    
                }
                if(e.getKeyCode()== KeyEvent.VK_LEFT){
                    logica.mover(Pacman.Direccion.IZQUIERDA);
                    pacmanp = 180;
                    
                }
                
                if(e.getKeyCode() == KeyEvent.VK_UP){
                    logica.mover(Pacman.Direccion.ARRIBA);
                    pacmanp = 90;
                }
                
                if(e.getKeyCode()== KeyEvent.VK_DOWN){
                    logica.mover(Pacman.Direccion.ABAJO);
                    pacmanp = 270;
                }
                    
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });
        try{
            image = new BufferedImage[4];
            image[0] = ImageIO.read(new File("src/imagenes/fondo.png"));
            image[1] = ImageIO.read(new File("src/imagenes/pared.png"));
            image[2] = image[0];
            image[3] = ImageIO.read(new File("src/imagenes/galleta.png"));
            
        }catch (IOException ex){
           Logger.getLogger(Animation.class.getName()).log(Level.SEVERE, null, ex);
        }
        logica.Leernivel();
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
       
        g.fillRect(0, 0, getWidth(), getHeight());
        
        for (int i = 0; i < logica.getFilas(); i++) {
            for (int j = 0; j < logica.getColumnas(); j++) {
                g.drawImage(image[logica.get(i, j)], j * pacmanTamaño, i * pacmanTamaño, this);
                
            }
        
        //for(int i = 0; i < logica;)
        
        
        // solamente lógica de dibujar
        g.setColor(Color.yellow);
        g.fillArc(logica.getPacman_columna() * 20, logica.getPacman_fila() * 20, pacmanTamaño,pacmanTamaño ,pacmanp+angulointerno , 360 -2 * angulointerno);
        
        if(logica.getpeloticas() == 0){  
              logica.nivel++;
              logica.Leernivel();
          }
        }
        
    }
    

    public void nextFrame() {
         if((angulointerno >= 30)||(angulointerno <= 0)){
            bocaAbierta = -bocaAbierta;
        
         }
        angulointerno += bocaAbierta;    

 
       
    }

    public void init() {
        if (this.timer == null) {
            this.timer = new Timer(10, new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    nextFrame();
                    repaint();
                }
            });
        }
        if (!this.timer.isRunning()) {
            this.timer.start();
        }
    }

    public void pause() {
        this.timer.stop();
    }

    public void restart() {
        this.pause();
        // Iniciar variables
        this.init();
    }

    
    //metodo get

  
  
    
}
