/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.uvp.pri.animation;


import java.util.ResourceBundle;

/**
 *
 * @author salasistemas
 */
public class Pacman {

    public ResourceBundle bunble = ResourceBundle.getBundle("niveles");
    public enum Direccion {ARRIBA,ABAJO,IZQUIERDA,DERECHA};
    public int[][] tablero;
    public int nivel = 1;
    private int pacman_fila;
    private int pacman_columna;
    int positionFileAnt ;
    int positionColumnAnt;
    int peloticas=0;
    int i,j;
    

    public Pacman() {

    }

    public Pacman(int[][] tablero) {
        this.tablero = tablero;
    }

    public void Leernivel() {
        String str = bunble.getString("nivel" + nivel);
        System.out.print(str);
        String[] valores = str.split(" ");
        int filas = Integer.parseInt(valores[0]);
        int columnas = Integer.parseInt(valores[1]);

        tablero = new int[filas][columnas];
        int k = 2;

        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                tablero[i][j] = Integer.parseInt(valores[k]);
                if (tablero[i][j] == 2) {
                    pacman_fila = i;
                    pacman_columna = j;
                }
                if (tablero[i][j] == 3) { // si hay una galleta la acumulo
                    peloticas= peloticas + 1;
                }
                    k++;

            }
        }
    }
    
    public int get(int i, int j) {
        return tablero[i][j];
    }
    public void mover(Direccion direccion){
        positionFileAnt = pacman_fila;
        positionColumnAnt = pacman_columna;
        
        try{
        switch (direccion){
            case ARRIBA : pacman_fila --;
            break;
            case ABAJO : pacman_fila ++;
            break;
            case IZQUIERDA : pacman_columna --;
            break;
            case DERECHA : pacman_columna ++;
            break;
        }
        
        if(tablero[pacman_fila][pacman_columna] == 1){
            pacman_fila = positionFileAnt;
            pacman_columna = positionColumnAnt;
            
        }
          if(tablero[pacman_fila][pacman_columna] == 3){
            i=pacman_fila;
            j=pacman_columna;
            this.tablero[i][j] = 0;
            peloticas --;
        }
        }catch(ArrayIndexOutOfBoundsException exc){
            int f = this.getPacman_fila();
            System.out.println("" + f);
            int c = this.getColumnas();
            System.out.println(""+ c);
            if(c>= 8){
                this.pacman_columna = 0;
                this.pacman_fila = 4;
            }
            if(c>= -1){
                this.pacman_columna = 7;
                this.pacman_fila = 4;
            }
            
        }
        
    }
    
    public int getFilas (){
        return tablero.length;
    }
    
    public int getColumnas (){
        return tablero[0].length;
    }

    public int getPacman_fila() {
        return pacman_fila;
    }

    public void setPacman_fila(int pacman_fila) {
        this.pacman_fila = pacman_fila;
    }

    public int getPacman_columna() {
        return pacman_columna;
    }

    public void setPacman_columna(int pacman_columna) {
        this.pacman_columna = pacman_columna;
    }
    
    public void setpeloticas (int i){
        this.peloticas = i;
    }
    
    public int getpeloticas (){
        return this.peloticas;
    }
}
